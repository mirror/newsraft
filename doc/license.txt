Copyright 2021-2024 Grigory Kirillov <txgk@bk.ru>
Copyright 2023-2024 Newsraft contributors (see git log)

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.

The project as it is right now would not be possible without the use of such
wonderful pieces of software as curl, expat, gumbo, ncurses, scdoc, sqlite and
yajl. The licenses under which these projects are distributed are provided in
files license-curl.txt, license-expat.txt, license-gumbo.txt,
license-ncurses.txt, license-scdoc.txt, license-sqlite.txt and license-yajl.txt
respectively.
