Before submitting your report, check if a similar issue has already been
discussed [here](https://codeberg.org/newsraft/newsraft/issues). If a similar
issue has already been reported, you do not need to create a new one.

In order to make a good thorough report, please follow these simple steps:

1. Run the program with `-l mylog.txt` in the arguments and reproduce the bug
you found. Please try not to do things that are not related to the bug so the
log will be more readable and compact.

2. In your report describe what happened in detail, what you think should have
happened and provide name and version of the operating system you are using.

3. If you are using some kind of network authentication, replace your passwords
in the `mylog.txt` with fake ones.

When you're done, [create issue](https://codeberg.org/newsraft/newsraft/issues/new)
for your report with your explanation and attached `mylog.txt` file.
