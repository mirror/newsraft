Package maintainers heavily rely on the format of the names for a release,
so releases must always comply with these rules:

Release commit tag name = newsraft-VERSION
Release title = newsraft-VERSION
